#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bottle import run
from blog import blog

app = blog.Application

if __name__ == "__main__":
    run(app, host="localhost", port="9090", debug=True, reloader=True)
